
package numberrangesummarizer_assessment;

import java.util.Collection;

/**
 *
 * @author lionel Mangoua
 * Date: 08/02/17
 */

public interface NumberRangeSummarizer {
    
    //collect the input
    public Collection<Integer> collect(int[] input);

    //get the summarized string
    public String summarizeCollection(Collection<Integer> input);
    
}
