/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numberrangesummarizer_assessment;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author lionel Mangoua
 * Date: 08/02/17
 */

/**
 * Implement this Interface to produce a comma delimited list of numb,
 * 
 * grouping the numb into a range when they are sequential.
 *
 * Sample Input: "1,3,6,7,8,12,13,14,15,21,22,23,24,31
 * 
 * Result: "1, 3, 6-8, 13-15, 21-24, 31"
 *
 * The code will be evaluated on
 * 
 *   - functionality
 * 
 *   - style
 * 
 *   - robustness
 * 
 *   - best practices
 * */

public class TestNumbers {
    public static void main(String[] args) {
        
        NumberImplementation numImp = new NumberImplementation();
        
        int num[] = {11, 1, 3, 5, 6, 8, 12, 14, 15, 16, 4, 22, 23, 24};
        
        //Print the given numbers
        System.out.print("Given number: ");
        for(int i = 0; i < num.length; i++) {
            System.out.print(num[i] + ", ");
        }
        
        System.out.println("\n");
        
        Collection list = numImp.collect(num);
        
        System.out.print(numImp.summarizeCollection(list) + "\n" + "\n");
                
        ArrayList newArray = new ArrayList();
        
        newArray = getRanges(num);
        
        System.out.print("Range : ");
        for(int y = 0; y < newArray.size(); y++) {
            System.out.print(" " + newArray.get(y));
        }
        System.out.println("\n");
  
    }
    
    //Get ranges
    public static ArrayList getRanges(int num[]) {
        
        ArrayList ranges = new ArrayList();
        
        int rstart, rend;
        int lastnum = num[num.length - 1];
        
        for (int i = 0; i < num.length - 1; i++) {
            rstart = num[i];
            rend = rstart;
            
            while (num[i + 1] - num[i] == 1) {
                rend = num[i + 1];
                
                //Increment the index if the numbers sequential       
                if (rend >= lastnum) {
                    break;
                } 
                else {
                    i++;
                }
            }
            if (rstart == rend) {
                ranges.add(rend + ",");
            } 
            else {
                ranges.add(+ rstart + "-" + rend + ",");
            }
        }
        return ranges;
    }
}
