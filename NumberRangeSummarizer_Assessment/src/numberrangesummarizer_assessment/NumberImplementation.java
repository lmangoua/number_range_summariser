/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numberrangesummarizer_assessment;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author C55-C1881 White
 */
public class NumberImplementation implements NumberRangeSummarizer {
    
    @Override
    public Collection<Integer> collect(int[] input) {
        Collection<Integer> collect = new ArrayList<Integer>();
        
        //Sort the array of numbers
        int i, j, temp;
        
        for(i = 0; i < input.length; i++) {
            for(j = i + 1; j < input.length; j++) {
                if(input[i] > input[j]) {
                    temp = input[i];
                    input[i] = input[j];
                    input[j] = temp;
                }
            }
        }
        
        //Assign the input array to the collection
        System.out.print("Sorted numbers in a list: ");
        for(i = 0; i < input.length; i++) {
            collect.add(input[i]);
        }
        return collect;
    }
    
    @Override
    public String summarizeCollection(Collection<Integer> input) { 
        //display a collection of integer numbers as string
        String sumCollect = input.toString();
        
        return sumCollect; 
    }
}
