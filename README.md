# Number_Range_Summariser

This Interface has to produce a comma delimited list of numbers, * grouping the numbers into a range when they are sequential. Sample Input: &#34;1,3,6,7,8,12,13,14,15,21,22,23,24,31&#34; * Result: &#34;1, 3, 6-8, 13-15, 21-24, 31&#34;. The program has been compiled using Netbeans 8 IDE.